Ciudad de Buenos Aires, 26 de marzo de 2020

Sr. Director del Hospital Parmenio Piñero, 

Por la presente quiero solicitar se revea la prohibición de ingreso que pesa sobre mi persona, lo que dificulta notablemente la posibilidad de darle de comer a mi madre que está internada en Clínica Médica, Unidad 1, Cama 18
  
En primer lugar hay que mencionar que mi madre, está internada esperando geriátrico, motivo por el cual ninguno de los dos medicos que tuvo hasta el momento quiere reconocer ninguna de las enfermedades que tenía con anterioridad y seguira teniendo estando en casa o el geriatrico. Desconozco el grado de demencia senil que pueda tener, pero eso también sirve de excusa para que digan que está inventando. Y para que venga el siquiatra tratante de PAMI es necesario que lo solicite el médico. 

Hasta el momento tuve 3 conflictos que derivaron en el llamado a Personal de Seguridad, y que si se tiene en cuenta que los enfermeros no responden al llamado del pulsador, sea porque no tiene sonido o lo tienen escondido y no pueden ver. Luego de esperar a veces dos horas otras veces una, uno se levanta y va a golpear la puerta de enfermería, pero la mayoría de los reclamos no pueden ser resueltos por ellos y la charla siempre termina con a) hablalo con el medico o b) andá a la Guardia.

Primer altercado

Fue por el notable deterioro que sufrió y sufre desde el cambio de la Unidad 4 a la 1, mientras estaba en la Unidad 4 tenia fuerzas para gritar, y hasta para bajarse una vez de la cama 
y desplazarse arrastrandose, estuvo "contenida" en varias ocasiones. En cambio en la Unidad 1 apenas tiene fuerzas para tragar.

Un día la vi bastante mal, abría muy poco los ojos y se notaba el sudor. Toque timbre (para que el enfermero pueda venir y ver con sus propios ojos) y nada. De nuevo, me acerque a enfermeria, consulté al enfermero si se podia conseguir un medico de guardia y me respondio no recuerdo literalmente pero algo entre "es un chiste?" y "me estas jodiendo!". Entonces le pregunte que hacia, llamo a PAMI o la llevo a la Guardia. Si, llama a PAMI. Pero PAMI no envia emergencias a un hospital. Entonces, ya que el enfermero NO PODIA ACERCARSE desde su "cuarentena" en la sala de enfermeria, y que no hay medicos desde las 13 (por lo general y peor ahora con el Corona).

Fui un par de veces a la Guardia pero con la situación de pandemia no me prometieron ni un medico para dentro de una hora. Entonces simplemente, si la atención medica no puede venir a un paciente internado, que el paciente vaya a la atención médica.

Lamentablemente las camas de internación no entran en el ascensor. Y hubo que emprender el viaje de vuelta, aunque de repente aparecerion CUATRO, no una, sino CUATRO medicas de guardia, que revisaron a mi mama, y me dijeron que me quede tranquilo y vaya a casa. Al otro dia, parece que con un mejor termómetro encontraron 38,5 de fiebre. Demasiado escandalo para detectar una fiebre, no hubo forma más fácil.

Un detalle de color es que la compañera de habitación de mi mama en la epoca en que hubo dias bastante frios abría se ponia un pulover, se envolvía en la frazada y abría la ventana que esta arriba de la cama de mi mamá, comente a una enfermera para evitar un conflicto de convivencia. Tampoco a nadie se le ocurrio decir que cierre la ventana. Y ya se por que, porque eso es cosa de los medicos, segun comento el enfermero.

Y algo divertido fue cuando me di cuenta que una de las personas que me mandaba a la guardia, hablaba como si no trabajara ahi era un enfermero recién llegado que aún no se habia puesto la ropa de trabajo.

Segundo altercado

Motivado por la flema. Si uno va a darle de comer y no se libera la flema, darle de comer es algo que causa mucha molestia al paciente, porque ahora la tos involucra flema y comida.

Intentamos el camino diplomático: tocar el pulsador pero a la hora uno ya pierde las esperanzas y va a golpear la puerta de enfermería, como era de esperar: "andá a la guardia", fui a la guardia pero no consegui nadie para que use el aparato de diafragma, toque timbre en la Unidad 2 a ver si me podian prestar un enfermero capacitado en ese aparato, pero me dijeron que solo médicos y kinesiologos pueden / deben manipularlos. Nuevamente a la guardia, nada; de nuevo hacia la habitación con la idea de darle algo caliente para que pase la flema. ¿Pero que pasó? ya eran las 15 y termino el horario de visita, asi que el enfermero simplemente no permitía el ingreso. Me hizo pasear por el hospital buscando un médico para aspirar la flema y ¿ahora jodete?. No se aspiro la flema ni se le dio de comer. Sacudí un poco la puerta y logre que venga Seguridad, que es un poco mas racional que el enfermero en cuestion, y al menos me dejaron pasar a retirar mis cosas. Durante este "conflicto" logré que el enfermero mueva el culo del asiento, asi lo comente en voz alta y lo consideraron una ofensa. Se podria mencionar que nadie se dio cuenta de la flema hasta que llegue a darle de comer. Incluso hacia ruidos raros hasta para tragar agua.

Aunque me olvide el cargador del celular, que supongo que la gente que participo en el desalojo habra visto y ya imaginara usted que pudo haber pasado con cosas de valor, de eso no me quejo, esta la nota sobre los objetos de valor. Mario se comprometio a darle de comer, pese a que el otro enfermero había dicho que no es responsabilidad de ellos. Más que enfermero parece simplemente el portero que atiende el timbre de la unidad.

Nuevamente, el mismo problema, un paciente con flema y poca movilidad (mientras estaba en Unidad 4 podía escupir y limpiarse) que podria ahogarse muy facilmente, y el enfermero que nuevamente, de NINGUNA FORMA intenta averiguar el problema, tratar de dar una solucion o respuesta empática y encima de no solucionar nada, se convierte en un cumplidor de normas que a las 15 ya no te deja pasar.

Tercer conflicto

El ultimo altercado fue el 25, fui temprano al Hospital para reclamar al médico por los problemas que mencionare mas adelante además de estos dos previos. No fui por el parte médico porque ya estaba avisado que mi mamá no es un paciente que necesite de partes. Asi que simplemente estuve esperando, llegaron las 12 y el médico no aparecía. Se me ocurrió revisar si habian tirado el cargador a la basura, pero encontre dos bandejas INTACTAS con comida, asi que las saque, fui a reclamar a Direccion (no me atendió nadie) y después hable con el Subdirector quien me dijo que iba a hablar con el Jefe de la Unidad.

Asi que la comida del día anterior fue descartada y el yogurt y la leche con 3 vainillas, seguían inmaculadas. Eso si que da mucha seguridad de como se iban a ocupar de darle de comer.

Finalmente a las 13 aparece el Doctor y viendo las bandejas de comida en una de las mesitas, procede a expulsarme por ser esa una acción que pone en peligro a la humanidad toda. Sacar comida de la basura de la habitación de mi mamá a mi no me preocupa mucho, si me da mas miedo que me manden a la Guardia donde esta bastante saturado de gente con barbijos. Creeria que es mas peligroso enviar a familiares de internados a la Guardia para que vuelvan y contagien al resto solo porque el enfermero no esta capacitado, a que haya sacado comida de la basura para que alguien me pueda creer que no le dan de comer a mi mama. Además mi mama estaba sola en la habitación, la cama 17 estaba vacia, asi que en la basura solo hay cosas de mi mama y de su habitación.

Esos son los 3 conflictos puntuales. Luego podria mencionar:

- mi mamá empeoró mucho desde que la cambiaron de Unidad, por que? No se sabe porque no dan partes de los que esperan geriatrico.

- la fiebre, 38.5... y la flema, es sintoma de algo? Quien sabe. Se enteró el médico?

- el muñeco de Michelin. Mi mama esta pareciendose por lo hinchado que quedan las partes donde colocan el suero. Pero por otro lado una vez estuvo 2 días perdiendo suero sobre la colcha y utilizando el simple metodo cientifico de poner una toalla seca bajo su brazo pude comprobar la perdida... y cuando cambiaron el suero... no goteaba!

- la hinchazon en la pierna, ¿es por el suero, por retención de liquidos o por trombosis?

- las moscas de la fruta que pueblan la habitación? la vecina compraba un kilo de bananas y arrancaba de a un dedo, dejando el resto para el criadero de moscas. Igual esas no pican pero son molestas porque se posan en cualquier lado y luego en la comida. ¿Nadie le dijo nada?

- Una vez tuve que dejar una nota: ¿está bien que una persona que no se hidrata por su cuenta no tenga puesto suero? Desaparecio el papelito y aparecio el suero. Genial, pero ¿lo hicieron por mi nota?

- Para que no digan que soy destructivo, la puertita de la mesita de la cama 17 la puse yo. Habia que ajustar dos tornillos... tarde casi 3 horas pero puede hacerlo.

- Si cada problema minúsculo va a requerir que un familiar se desplace a la Guardia, me parece de lo menos eficiente y de lo más peligroso, considerando que en la Unidad podría haber un telefono para casos de Urgencia.

Por todo lo anterior es que solicito se revea la prohibición, ya sea porque 1. los medicos no dan prioridad a pacientes en espera de geriatrico, 2. no tengo seguridad de que le den de comer (y tampoco podrían (NI DEBERIAN) dedicarle el tiempo que puedo decicarle yo), 3. basandome en 1 no creo que al medico le preocupe mucho si come o no y 4. tengo que ocuparme de estar alerta al suero, hematomas, hinchazones, trombosis, etc.

Una solicitud adicional sería que se la cambie a la Unidad 4 donde en base a la experiencia de casi 50 días con los enfermeros podria quedarme tranquilo que se le dará de comer y donde, quiza por cuestion de horarios, veía un mayor control sobre los pacientes.

También podrían intentar con el centro Coordinador de PAMI que se traslade a mi mamá a un hospital propio de PAMI donde se puedan tratar el resto de las patologías existentes. Y aca hay un problema adicional, si no puedo ingresar al hospital, no puedo acercarme al Centro Coordinador para reclamar a PAMI por la atención que tiene el afiliado.

En caso de que se me vuelva a permitir el ingreso, me gustaría se considerara también el cambio a la Unidad 4. Y en caso negativo, solicitaría una nota por escrito para cualquier eventualidad legal que pueda surgir.

PD: dejo un enlace a la versión online, intentaré ir subiendo las fotos y videos mas adelante: https://gitlab.com/kosciuk/escritos/-/blob/master/pinero.md

--

Actualización 26 de marzo 2020

Se llamo al MPF de la Ciudad, no se pudo encuadrar en ningún delito, se quedó en que concurra mañana y si no se permite el ingreso que llame desde el lugar.

En la Defensoría de la Ciudad se preocuparon un poco más, y llamaron al Hospital, consiguieron que pueda hablar "calmadamente" con alguien, que espero sea el Director. Se verá mañana.

Actualización 27 de marzo 2020

Llame a la Dirección General de Hospitales del GCBA a cargo de Auger, que creo que era Jefe de Cardiología en el Santojanni, y me respondieron con "presente una nota en Dirección". Lo mismo que dijeron en Dirección del Hospital: deja una nota en Dirección... y cruza los dedos para que el paciente no muera de hambre mientras esperamos el proceso burocrático, que encima puede ser retrasado por el Hospital y con la excusa del Coronavirus tienen excusa de sobra.

No se entiende para que existen los organigramas si nadie tiene poder sobre nadie.

--

Habre llegado al hospital 11:20, avise a Seguridad que Defensoria habia hablado con el Hospital y habian arreglado de que podia ir a hablar con alguien mientras sea civilizadamente.

Espere unos 20 minutos en Direccion y cuando salio el Director le dije lo mismo, "que me dijeron que vaya para hablar con alguien", pense que hablaría con él, pero no, el Director me dijo que vaya al pabellon donde esta mi mama y hable con la jefa de piso y se fue. Nunca me dijo que tenia permiso y que vaya directo a ver a mi mama. Por lo que volvi a Direccion, comente eso y me dijeron que no sabian nada, que nadie habia llamado y que lo que yo pueda decir que dijo el director no alcanza. Yo sin permiso formal no me voy a meter en el Pabellon para dar excusas de que soy un violento.

Entonces llame nuevamente a la Defensoria. en el medio de la espera vi el mail donde decian que "habian hablado con el Director y no habria problemas en que vea a mi madre" y me dijeron que mostrando el mail no tendria problemas. Busque al Director, le mostre el mail y me dijo que no habria problema mientras me porte bien.

Con la confianza de que podria entrar mostrando la nota del celular, voy al pabellon, pero los de Seguridad no confiaron en el permiso, asi que solo me acompañaron a que hable con la Jefa de Piso, no la encontramos, pero si encontramos a la Jefa de Enfermeria de la Unidad 1, que dijo que eso lo autoriza el médico de Planta (que seria el jefe de la Unidad) y que ya se habia ido, que venga el lunes. Asi que no patalee y me fui.

-- 

A las 14 del dia de la fecha, me llamaron de la Defensoria, me dijeron que me acercara al Hospital porque el Subdirector Pereira (o Pereyra) me esperaria hasta las 15 para darme un permiso por escrito.

Llegue al Hospital a las 14:30, espere casi hasta las 15 cuando por fin apareció el Subdirector, no parecia estar muy al tanto del problema, pero se acordaba bien de mi porque me dijo que yo ya le habia hablado y tenia las bandejitas con comida. Me dijo que el no podia darme ningun permiso y que lo solicitara al medico de piso (el mismo que me hizo echar) y el lo iba a refrendar. ¿Si el mismo medico que me echo me deja volver a entrar, para que necesitaria que lo firme alguien mas?

Lo tragico es que el Subdirector estaba muy apurado y parecia estar preocupado porque "se le estaba muriendo gente", pero apenas comente que no consegui nada a la Defensoría, intentaron comunicarse con el pero ya se habia ido. Creeria que el apuro era mas bien en irse.

Esperaria que la Defensoria se ponga un poco mas firme y deje de creer lo que dicen desde el Hospital, ojala se pueda dar intervención a un Fiscal o alguien que pueda concurrir en persona y tomar nota, ya no tanto por mi, para que me dejen ingresar, sino para asegurar los cuidados mínimos de la paciente internada.