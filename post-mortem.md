Muerte en Hospital Piñero

El presente texto en una historia más completa sobre el fallecimiento de
mi mamá en el Hospital Piñero, usando como base la nota que presente en
el Hospital para que se me permita el ingresar a darle de comer.

Creería que todo empieza con un simple dolor en la cadera que le impedia levantarse a orinar 
durante la noche.

Anterior a esto, aunque deliraba simpáticamente, por ej: creía que la gente de 
la televisión se habia metido en la casa y que habia que llamar a la 
policia, otras veces creía lo contrario, si los periodistas le caian bien 
se preocupa en donde iban a dormir. Pero en cuanto a salud "clinica" andaba
bien del corazon, tenia presion baja, estaba anticoagulada simplemente 
como dijo un medico: "no sabe manejar el dolor", por eso que llamaba al 139 tan seguido

Como los turnos de PAMI son a 2 o 3 meses, entonces no quedo otra que
esperar a que termine con una infección que permita el traslado.

Por algun motivo PAMI puede enviar 400 ambulancias por año pero no puede
solucionar un problema concreto que incluso evite las llamadas a PAMI.
Es muy sospechoso ese presupuesto en ambulancias.

Con la primera visita a la Guardia del Piñero, el 27 de diciembre (creo), 
hicieron analisis de sangre y el resultado de la muestra de orina confirmo 
la infeccion urinaria. Nos recetaron un antibiotico que nos entregaron gratis
en la farmacia del hospital, envolví a mi mamá en una frazada y la 
arrastre como pude para subirla al taxi.

Se termino el tratamiento del antibiotico sin ninguna mejoría, hasta que un día un médico 
del 139 dijo que la infección era pulmonar (y ahora se sabe que este le acertó),
receto otra medicación que hizo bien los primeros dias pero despues quedo como antes.

Desde que vino este doctor hasta que se la interno la segunda vez, le pregunte a cada uno de los
medicos que vinieron que veian en el analisis de laboratorio y mitad decia que era infeccion urinaria y 
la otra mitad decia que no. Como ya se dijo mas arriba, se llamaba muchas veces a PAMI, con lo que 
muchas veces terminaba hablando con la supervisora, le comente que mitad decia una cosa y mitad decia otra y 
me respondio con astucia si llamaba a los medicos para hacer encuestas, jamas 
le iba a importar que mitad de los medicos sepan interpretar un resultado.

Hubo varios días en los que se cayó, un dia fueron 3 veces pero de culo (es decir, sin daños), una vez
de cabeza, tambien con suerte: tardó dos días en quejarse de que lo dolía la nariz. 
En otra de esas caidas, rompio la base del ventilador.

Lo que si pude conseguir fue la internación domiciliaria, aunque no asignaron el modulo del cuidador domiciliario por 8 horas que se pidio (ni el de 4). 
La enfermera vino un sabado, la kinesiologa el lunes y cancele todo antes de que pudiese venir el medico, ya que no se podian cancelar cosas individualmente, 
era todo o nada. Cancele todo, ya bastante con mi mama, para tener que hacerme cargo de 3 personas mas.

Seguimos llamando al 139, también a Salud Mental, hasta el 19 de enero en que la quinta ambulancia
que vino decidio internarla por un fuerte dolor abdominal. Nuevamente analisis de sangre y orina. 
Esta vez, el analisis de orina dio igual al anterior, pero para este medico no era indicacion de nada.
Carajo! Antes fue la confirmación de la infeccion urinaria y ahora no!

Pensando que en el hospital iba a estar mejor que en casa, viendo que era domingo de noche,
sabiendo que el faltaban dos dias para el martes (dia junto al jueves en que la 
Agencia de PAMI entrega sus miserables 15 numeritos para la seccion de Asistencia Social) dije, es ahora o nunca,
deje mi numero y me fui a casa. Por fin podria ir personalmente montar guardia desde las 5 de la mañana en la Agencia 2.

El lunes me llamaron diciendo que tenían a una paciente con esquizofrenia, me parecio un poco exagerado. La pasaron 
de las salas de guardia a las de internación de Guardia, como cuando se va hacia el Shockroom a la derecha. En ese traslado ya desaparecio una almohada.

El martes no pude madrugar todo lo que quería pero logre conseguir el numero 5, al comentarle que estaba internada en el Piñero me hizo devolver el número y me aviso que el tramite
se hace en el Centro Coordinador que se encuentra en el Hospital.
Ese mismo día fui pero la encargada de Asistencia Social estaba ausente por enfermedad, al otro dia ya pude iniciar los preparativos.

Estuvo uno o dos dias en Guardia hasta que la trasladaron al Pabellón II de 
Clinica Médica, Unidad 4, Cama 4, junto a Nora, con la que siempre 
cantabamos cuando me iba al mediodía y a la noche:

"Un dia al cielo iré,
y la comtemplaré,
la gloria de María,
dichoso cantaré"

Y después seguía: "Amigo mío que has venido a ver a tu madre...", por suerte estaba bien, 
habia tenido una caida y tenia que practicar para poder volver a caminar normalmente. Ella se ocupa
de cuidar a mi mamá, llamaba con el pulsador para que la tapen y me contaba el "mantenimiento" que 
le habían hecho. Era un gran alivio tener una "infiltrada" en la habitación de mi mamá.

En su estancia en cama 4, la trataron por neumonía (por eso dije mas arriba, este medico si que la pego) y 
andaba bastante bien, tenia tanta vitalidad que podía arrancarse las vías, incluso afirman haberla vista tratando 
de escaparse como un kayakista sobre su pañal.

En muy pocos días la doctora que la trataba me dijo: "lo pedís, lo tenés" y llevo los papeles para solicitar 
la internación. Eso fue sorprendente. Me queda por averiguar en que fecha fue esto, porque para firmar el 
papel de PAMI en el que aceptaba el descuento del 80% de los haberes fue MUCHO despues, y 
supongo que durante todo ese tiempo, no se estuvo buscando nada.

Pasadas unas dos o tres semanas, dejaron en aislación a mi mamá, moviendo a Nora a la cama 1, diciendo que mi
mamá tenía "unos bichitos en la caca" y por eso deberia aislarse, se pegó un papel en la puerta (nota mental: buscar foto).

Mientras estuvo en la Unidad 4, solo tuve problemas con el Jefe de Unidad, el Dr. Copani, porque parece ser que
-todos los pacientes de PAMI a la espera de Tercer Nivel (geriatrico) son tomados por los jefes de piso", 
¿por qué? Supongo que son los casos más simples y si el paciente muere, "es culpa de PAMI que no consiguió el traslado", 
el paciente como dijo el Doctor "está de alta y te la podés llevar. Querés llevartela?". 
NO! Para que se rompa la cabeza en casa, no. 

El paciente está de alta, pero no se le pueden dar los remedios que tomaría normalmente. El médico en lugar de
decir: "ya que la mujer está de alta pero va a seguir acá, veamos que remedios venía tomando a ver con cuales 
se puede retomar el tratamiento", dice: "". Quise decir que no dice nada. Y si la paciente tiene principio de
demencia, es casi como un milagro, porque desaparecen todas las enfermedades: "hay que ver que cosas esta 
inventando y que cosas no". 

- Genial! ¿Significa eso doctor que va a llamar al siquiatra tratante? 
- No, no, significa que son las 12:30 y tengo que abandonar el edificio.
 
Así empezaron los conflictos con el Jefe de Unidad, porque el negaba los problemas gastrointestinales 
de mi mama diciendo que eran inventados, pero yo no podía solicitar al prestador de cartilla 
que venga a demostrar lo contrario porque "solo lo puede pedir el médico tratante".

¿Y el médico tratante va a llamar a alguien para que le diga que algunas cosas no son inventadas?

Y algunas cosas no inventaba, por ejemplo, estuvo una semana diciendo que le "me duele el ano", y 
tenia problemas para evacuar (lo que normalmente se dice cagar), 
le consulte por eso mismo al doctor en la puerta de la habitación (un dia fui temprano, golpee 
en la sala de medicos y avise que venia por la Cama 4, con la intención de que hacer que se tenga que acercar y ver). 
Y dijo que estaba inventando eso. Días despues, ojala conste en la historía, mi mama procedio 
a la remoción de la (cuidado gente impresionable) mierda con sus propios medios (digamos dedo indice) y 
creo que no volvio a quejarse de eso (al menos en mi presencia).

Tampoco podía conseguir la historia clínica, algún medico me dijo que vaya a Admisión y Egresos, pero ahi me dicen 
con innegable lógica: si la paciente esta internada, la historia está donde esta ella. Raro que no sepan eso. Solicite 
la historia clinica al medico, pero me dijo que vaya a Dirección Médica, que aun no pude averiguar donde es, aunque cuando me vio saliendo de Dirección me dijo que ahi no era. Se ve que jugaba a las escondidas, en lugar de decir donde quedaba eso de Dirección Médica, solamente me decia donde no era. Deje mi reclamo en el libro de quejas, hubo una respuesta pero ni me intereso leer lo que me iban a dar.

Como el médico no hacía nada por el constante dolor de estómago, que a la larga hace que se ponga nerviosa
y se genere un circulo vicioso en que ya le empieza a doler mas porque si, le di de los Lexotanil que tomaba en casa (que no hacian gran efecto despues de tomar cerca de 20 años) y con tanto tiempo sin tomar, le afecto demasiado. Esa fue una macana mía.

Por ese motivo, me incautaron los remedios, incluso los que debería poder tomar si es que estaba de alta y que
tenia recetados con anterioridad, como el Rogastril Plus.

Finalmente aparecio el coronavirus y la decadencia de Doña Margarita: fue traslada a la Cama 18 de la 
Unidad 1, para hacer lugar en la Unidad 4 para casos de corona.

Luego del traslado, intenté recuperar los remedios retenidos, y paso algo curioso, una doctora que en cualquier
otra situación hubiese dicho, "habla con el médico de tu mama, yo no se nada", dijo: "los tiramos a la basura
porque estaban vencidos". ¿Que carajo se tiene que meter con su defensa corporativista? No te van a decir 
donde esta el baño porque tienen que ir corriendo a la guardia, pero se van a meter a
decir cualquier verdura (o fruta de estacion) si se trata de defender a un colega.

Y en esta unidad también empezamos mal con el Jefe, ya de entrada me dijo que había gente esperando geriatrico mas de 1 año, que estaba el coronavirus y pacientes con tuberculosis! También me dijo que sabía que tuve "problemas" con Copani, el chusmerío viaja mas rapido que las historias clinicas.

El principal inconveniente de la Unidad 1, es que los enfermeros no se enteran de cuando se toca el pulsador, parece ser que el aparato que esta en un lugar no visible y no tiene alarma sonora (como si tenian en la U4), pueden pasar dos horas sin que venga nadie. Es raro, porque tambien pasaban caminando por el pasillo y tendrían que ver la lucecita que se enciende sobre la puerta de la habitación.

Una día pregunte a un enfermero, ¿que podria haber pasado que toque el timbre hace 45 minutos y no vino nadie? Me dijo: "deben estar todos ocupados con otros pacientes", a lo que retruque ¿entonces usted me dice que si golpeo la puerta de enfermeria no va a salir nadie? Para que discutir sobre algo que se puede comprobar, fui a enfermería, golpee y salió un enfermero. Después, el primer enfermero, me dijo que el no era de esa unidad.

Cuando apenas fue traslada, tenía una compañera de habitación y la acompañante que en los días que hacía frío, no tenían mejor idea que abrir la ventana y envolverse en frazadas. Mi mama en cambio estaba bajo la ventana con su remerita. Intente que una enfermera haga el favor de explicarle que no abra la ventana para no tener el conflicto yo que tendria que convivir con ellas. No paso nada.

Incluso cuando se fueron nos dejaron las moscas de las frutas porque no se les ocurria dejar la fruta en una bolsa... y creo que se llevaron una Biblia, el MPF tiene la denuncia...

Como redacte en la nota que deje en Dirección, tuve 3 conflictos en esa Unidad.

El primero. Se notaba que mi mama no estaba bien, asi que toque el pulsador, espere un ratito y fui a enfermería, el dialogo fue algo asi:

- Buenas, se podrá conseguir un médico?
- ¿Me estas jodiendo?
- No, ¿entonces que hago? ¿La llevo a la guardia?
- Hace como quieras.
- Gracias

Cuando termine de sacar la cama con mi mama al pasillo de la unidad, los enfermeros ya habian conseguido notificar todo para evitar responsabilidades, "podes llevartela bajo tu responsabilidad".

Así que abrimos las puertas y emprendimos el viaje, no pudimos llegar muy lejos, las camas de internación son más anchas que las de traslado y nos atascamos en el ascensor. Vino alguien de seguridad al que ignore por completo, deje a mi mama estacionada en el pasillo del primer piso y fui a buscar mis cosas antes de que me desajaran. Afortunadamente aparecieron CUATRO doctoras, no una, sino cuatro. La volvimos a ingresar a la habitación, me hicieron esperar afuera mientras la revisaban, me aseguraron que todo estaba bien. Había que creerles, por lo que me fui.

Al otro dia, después de la comida, como dije que no habia traido el termometro, el enfermero entendió la indirecta y le tomo la temperatura: 38,5! Creería que ya tenía esa temperatura cuando la revisaron las 4 doctoras, y también tuvo tiempo de revisarla el medico tratante, si es verdad que pasan habitación por habitación por las 9 de la mañana.

Segundo conflicto. Mediodía, horario de la comida. Pero antes de darle de comer, necesito poder sacarle la flema acumulada, esta el aparato a diafragma en la mesita al lado de la cama. Mecanicamente: "pulsar, esperar tiempo prudencial, golpear puerta enfermería":

- Buenas, necesitaría que alguien aspire la flema.
- Te sugiero que vayas a la Guardia a buscar un médico.

En la Guardia no me dieron ningún médico, me mandaron a la vuelta, por la entrada que da a Varela a buscar el jefe de Guardia, pero los de Guardia no me hicieron la gauchada "estan todos ocupados", aunque alguien me sugirió averiguar en otra unidad, asi que intente que me presten alguien que sepa usar ese aparato en la Unidad 2, la de enfrente. Ahi me aclararon que ese aparato solo lo pueden usar médicos y kinesiologos. Pero la comida llega despues de que los medicos se fueron. ¿Entonces?

Pense, vamos a darle un te a ver si podemos hacer bajar esa flema. Pero el chistoso del enfermero me dijo: "ya son mas de las 15, terminó el horario de visita". Estuve unos minutos tocando timbre sin parar y finalmente tuve que sacudir la puerta para que llame a alguien de seguridad con mayor entendedera que el enfermero. Yo tenía permiso para darle de comer y el horario de visita no aplicaba a mi, sino tampoco le podría ir a dar de comer a la noche. 

Me dejaron sacar mis cosas, salvo el cargador del celular que me olvide. El de seguridad me dijo que venga mañana, pero no la puedo dejar sin comer a la noche, el enfermero mismo me dijo que no era responsabilidad de ellos darle de comer. Aunque el otro enfermero, Mario, dijo: "yo me ocupo". Ok, te creo. Me fui.

Tercer conflicto. Aunque es una continuacion del conflicto anterior. Esto fue el 25 de marzo, fui temprano para poder hablar con el Jefe de Unidad, ya que eran 5 dias sin el que médico se entere de nada. El cargador del celular no estaba, revise por todos lados, y se me ocurrió: "quizá en lugar de robarselo, simplemente lo tiraron a la basura". Y revisando la basura es que encontre comida de la noche anterior INTACTA, es decir, nadie clavo un tenedor o una cuchara con la intención de decirle: "tome Margarita, abra la boca", no, asi como la dejo el personal de cocina, de la misma forma la descartaron.

Saque la bandeja de comida, se la lleve al Subdirector, quien dijo que iba a hablar con el jefe de Piso. Me volvi a la habitación y deje la bandejita en la mesa donde dejan la comida, esperando a que venga el médico, pero nunca vino, finalmente la Jefa de Enfermeria me insto a que tire la bandeja, dije que no y aviso al Jefe de Piso, quien por fin apareció... para echarme. 

Nunca pude decirle que habia dias que estaba sin suero, otras que el suero no goteaba (luego de dejar la notita de que como puede ser que este sin suero alguien que no se hidrata sola), otras que el suero perdia y mojaba las sabanas, y nadie se daba cuenta. Tampoco pude averiguar si la pierna hinchada era por trombosis o por una via mal puesta.

Ese fue el ultimo día que entre en la habitación. Intente hacer una denuncia en la comisaría de que no podía ingresar a dar de comer a mi mama, pero me dijeron que mejor llame al 911 desde el Hospital y pida por el comisario. Esa misma noche hice eso, y se apersonaron unos 20 policias entre patrulleros y motos. El comisario a cargo, la excepcion a lo que me tiene acostumbrado la policía, me dijo: ¿dejaste una nota?, pues... no.

La nota la presente el 26 de marzo, pueden ver la versión original en (link), pero me advirtieron que posiblemente iba a pasar mucho tiempo por la situación de pandemia.

El Centro cooordinador de PAMI ya estaba cerrado, asi que tire una copia por debajo de la puerta para que se enterasen en algun momento. Afortunadamente, ese mismo día, me llamo la chica de Asistencia Social y le pude contar lo sucedido y le pase el link de la nota que había dejado.

También envie mail al Ministerio Publico Fiscal el 26 de marzo con titulo: "No me dejan ingresar al Piñero a dar de comer a mi mama" me respondieron con: 

"Denuncias MPF CABA <denuncias@fiscalias.gob.ar> 26 mar. 2020 13:00 
Estimado vecino:
En mi carácter de funcionario de la Oficina Central Receptora de Denuncias del MPF tengo el agrado de dirigirme a Ud. en respuesta al e-mail que nos enviara, a los efectos de informarle que para poder procesar su denuncia necesitamos imperiosamente contar con sus datos filiatorios completos, en especial, su dirección y teléfono de contacto para poder comunicarnos y así ampliar sus dichos. Su mail quedo registrado en nuestro sistema Kiwi bajo orientación n°271963.
...
bla bla bla
...
Felicita Villalba Diaz
Prosecretaria
"

Te llaman por teléfono y te explican por qué no pueden tomarte la denuncia, si fuese por escrito, podría dejar testimonio aquí. No se pudo encuadrar en ningún delito, se quedó en que concurra mañana y si no se permite el ingreso que llame desde el lugar al MPF. Hubo un par de correos mas pero sin que respondan nada que valga la pena transcribir.

El ultimo mail al MPF lo envié el 2 abr. 2020 14:00, con este divertido texto:

"Alguna novedad? Si no, ¿que puedo hacer? ¿intento ingresar corriendo, esquivando a seguridad, para que llamen al 911 y se judicialice?"

Nunca más respondieron.

Con la Defensoria de la Ciudad hubo mejor suerte, y llamaron al Hospital, consiguieron que pueda hablar "calmadamente" con alguien, que espero sea el Director. Se verá mañana.

El 27 de marzo 2020 llame a la Dirección General de Hospitales del GCBA a cargo de Auger, que creo que era Jefe de Cardiología en el Santojanni, y me respondieron con "presente una nota en Dirección". Lo mismo que dijeron en Dirección del Hospital: deja una nota en Dirección... y cruza los dedos para que el paciente no muera de hambre mientras esperamos el proceso burocrático, que encima puede ser retrasado por el Hospital y con la excusa del Coronavirus tienen excusa de sobra.

No se entiende para que existen los organigramas si nadie tiene poder sobre nadie.

Habre llegado al hospital 11:20, avise a Seguridad que Defensoria habia hablado con el Hospital y habian arreglado de que podia ir a hablar con alguien mientras sea civilizadamente.

Espere unos 20 minutos en Direccion y cuando salio el Director le repeti, "me dijeron que venga para hablar con alguien", pense que hablaría con él, pero no, el Director me dijo que vaya al pabellon donde esta mi mama y hable con la Jefa de Piso y se fue. Nunca me dijo que tenia permiso y que vaya directo a ver a mi mama. Por lo que volvi a Direccion, comente eso y me dijeron que no sabian nada, que nadie habia llamado y que lo que yo pueda decir que dijo el director no alcanza. Yo sin permiso formal no me voy a meter en el Pabellon para dar excusas a que llamen a la Policía.

Llame nuevamente a la Defensoria. en el medio de la espera vi el mail donde decian que "habian hablado con el Director y no habria problemas en que vea a mi madre" y me dijeron que mostrando el mail no tendria problemas. Busque al Director, le mostre el mail y me dijo que no habria problema mientras me porte bien.

Con la confianza de que podria entrar mostrando la nota del celular, voy al pabellon, pero los de Seguridad no confiaron en el permiso, asi que solo me acompañaron a que hable con la Jefa de Piso, no la encontramos, pero si encontramos a la Jefa de Enfermeria de la Unidad 1, que dijo que eso lo autoriza el médico de Planta (que seria el jefe de la Unidad) y que ya se habia ido, que venga el lunes. Asi que no patalee y me fui.

A las 14 del mismo 27 de Marzo, me llamaron de la Defensoria, me dijeron que me acercara al Hospital porque el Subdirector Pereira (o Pereyra) me esperaria hasta las 15 para darme un permiso por escrito.

Llegue al Hospital a las 14:30, espere casi hasta las 15 cuando por fin apareció el Subdirector, no parecia estar muy al tanto del problema, pero se acordaba de mi: "vos ya me habias hablado, tenias las bandejitas con comida". Me dijo que el no podia darme ningun permiso y que lo solicitara al medico de piso (el mismo que me hizo echar) y el lo iba a refrendar. ¿Si el mismo medico que me echo me deja volver a entrar, para que necesitaria que lo firme alguien mas?

Lo tragico es que el Subdirector estaba muy apurado y parecia estar preocupado porque "se le estaba muriendo gente", pero apenas comente que no consegui nada a la Defensoría, intentaron comunicarse con él pero ya se habia ido. Creeria que el apuro era mas bien en irse.

Seguimos dando vueltas con la Defensoría intentando ubicar a Margarita Garcet, Jefa de Piso,

Finalmente el 15 de Abril pude ubicarla donde todos me decian que debia estar (en la Unidad 4) y me dijo que "no había problemas" pero ahora había una nueva orden del Ministerio y no podría ingresar de todas formas ahora si por motivos del coronavirus. Hasta ese momento, los familiares debían ir a dar de comer y retirarse al finalizar, confirmando lo que dijo el enfermero: ellos no se ocupan de dar de comer. Aprovechando que estaba a unos metros, fui a ver si encontraba al Jefe de Unidad para ver si me dejaría pasar una vez terminada la nueva prohibición, 
el médico estaba hablando con una mujer, espere pacientemente.

- Hola, se acuerda que me echo hace un tiempo?
- No, yo no fui.
- Bueeeeeno, los de seguridad entonces.
- Eso si, puede ser

No queria reconocer que me había echado, pero sabía quien era y asi que encendió el caset: "su madre está bien, está esperando tercer nivel y mire esto". Señalo el papel con 
la resolución del Ministerio de Salud, que para el decía que nadie podia ingresar, ni siquiera las personas que debian ayudar a comer a los internados.

Supuse entonces que con la nueva resolución estarían obligando a los enfermeros a ocuparse de la alimentación de los internados, con lo que me quedaba mas tranquilo a que si dependiese de los Jefes de Unidad, pero por las dudas, ese mismo diafragma
escribi al Ministro de Salud del GCBA, Don Fernán González Bernaldo de Quirós <fgbquiros@buenosaires.gob.ar> con asunto: "Prohibición de visitas y desnutricion de los internados", el cual sigue aún sin respuesta.

También el 15 de abril hice una nueva denuncia en el MPF de CABA a ver si podían averiguarme si en la Unidad 4 dejaron morir de hambre a una mujer, el 

parte del mensaje decía así:

"En el pabellón 2 (Clinica Medica) del Piñero, Unidad 4 (habitacion 17 y 18) habia 2 mujeres, Nora, y otra que estaba muy flaquita y me pedia que le levante la pierna cuando colgaba de la cama (no tenia fuerzas), ahora bien, Nora esta en la cama 3 (mi mama estuvo un tiempo con ella en cama 4), y la otra mujer no se... mi teoria es que finalmente murió y cuando eso paso, movieron a Nora a la cama 3 para evitar que vea lo que pasó. Igual ya con ver el estado de abandono que tenia era como para reclamar que le pongan una sonda gastrica... o no se. Yo se que se resistia a comer, o comia poco y nada.

Me interesa denunciar eso, porque mi madre esta en un riesgo similar."

Esto se resolvió asi:

- ¿Sos familiar?
- No
- Entonces, no hay nada que podamos hacer

El 28 de abril hubo un llamado raro a las 0:55, número privado asi que no atendí, durante el resto del día no hubo otros llamados de números privados, y el 29 a las 10:22 me avisaron de Servicio Social que el Jefe de Unidad solicitaba mi presencia de manera urgente. Ya me imaginaba algo malo, casi no pude subir las escaleras y todo terminó con el comienzo: "Lamentablemente ...". Me mando a Admisión y Egresos y ahí me ofrecieron el entierro gratuito, aun no puedo creer la increible oferta, que como hijo, no me da más que sospechas.
